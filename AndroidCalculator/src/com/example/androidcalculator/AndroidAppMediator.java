package com.example.androidcalculator;

import mediator.I_Mediator;
import mediator.I_Model;
import mediator.I_Presenter;
import mediator.I_View;
import mediator.Mediator;
import model.CalculatorModel;
import view.I_CalculatorView;
import presenter.CalculatorPresenter;

import android.app.Application;
//Application es un envoltorio que permite controlar la aplicaci�n.
public class AndroidAppMediator extends Application {	
	private I_Mediator _mediator;
	//Adaptamos el mediador al resto de elementos.
	//Test del branch
	@Override
	public void onCreate() {	//En Android no creamos en el constructor sino en onCreate.
								//De esa forma cuando todo est� comprobado lo crea, porque se
								//ejecuta nada m�s ser creado.
		_mediator = new Mediator();
	}

	public void register(I_View view) {
		CalculatorModel model = new CalculatorModel();
        CalculatorPresenter presenter =
          new CalculatorPresenter(model, (I_CalculatorView) view);
		
        _mediator.registerMVPInstances (model, view, presenter);
	}

	public I_Presenter getPresenter(I_View view) {
		return _mediator.getPresenter(view);
	}

	public I_Model getModel(I_Presenter presenter) {
		return _mediator.getModel(presenter);
	}

}
