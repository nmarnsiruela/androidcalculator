package com.example.androidcalculator;

public interface I_CalculatorView {
	public void display(String text);
	public void displayWarning(String text);
}
