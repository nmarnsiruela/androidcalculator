import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import mediator.I_Model;
import mediator.I_View;
import mediator.I_Presenter;
import mediator.I_Mediator;
import mediator.Mediator;

import static org.mockito.Mockito.*;  // Required for mockito

public class MediatorTest {
	private I_Mediator  _mediator;
	private I_Model     _model;
	private I_Presenter _presenter;
	private I_View      _view;

	@Before
	public void setUp() throws Exception {
		_mediator  = new Mediator();
		_model     = mock (I_Model.class);
		_view      = mock (I_View.class);
		_presenter = mock (I_Presenter.class);
		
		_mediator.registerMVPInstances (_model, _view, _presenter);
	}

	@Test
	public void testGetPresenter() throws Exception {
		assertEquals(_presenter, _mediator.getPresenter(_view));
	}

	@Test
	public void testGetModel() throws Exception {
		I_Presenter p = (I_Presenter) _mediator.getPresenter(_view);
		assertEquals(_model, _mediator.getModel (p));
	}

	@Test
	public void testUnregisterMVP() {
		try {
			_mediator.unregisterMVPInstances(_view);
		} catch (Exception e) {
			fail("view not found");
		}

		I_Presenter p = (I_Presenter) _mediator.getPresenter(_view);
        assertTrue (p == null);

		I_Model m = (I_Model) _mediator.getModel(_presenter);
        assertTrue (m == null);
		
	}
	
//	@Test
//  [ExpectedException("EmptyNameException"),
//	                   ExpectedMessage="The name cannot be empty")]
//	public void test() {
//		assertTrue(_viewMock instanceof I_View);
//		assertTrue(_presenterMock instanceof I_View);
//	}

}

