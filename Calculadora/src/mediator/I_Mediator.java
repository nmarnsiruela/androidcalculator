package mediator;


public interface I_Mediator {
	//Permitir� la opci�n de saltar de una pantalla a otra. Registramos la terma de MVP
	public void registerMVPInstances(I_Model m, I_View v, I_Presenter p);
	public void unregisterMVPInstances(I_View v) throws Exception;
	
	public I_Presenter getPresenter(I_View v);
	public I_Model getModel(I_Presenter p);	
}