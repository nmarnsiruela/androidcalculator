package mediator;
import java.util.ArrayList;

public class Mediator implements I_Mediator {
	private ArrayList<MVP_Components> _components;

	public Mediator() {
		_components = new ArrayList<MVP_Components>();
	}

	private class MVP_Components {
		private I_Model     _model;
		private I_View      _view;
		private I_Presenter _presenter;

		private MVP_Components(I_Model m, I_View v, I_Presenter p) {
			this._model = m;
			this._view = v;
			this._presenter = p;
		}

		private I_Model get_model() {
			return _model;
		}

		private I_View get_view() {
			return _view;
		}

		private I_Presenter get_presenter() {
			return _presenter;
		}
	}

	// ====================== Visible methods (for objects)
	
	@Override
	public void registerMVPInstances(I_Model m, I_View v, I_Presenter p) {
		getComponents().add (new MVP_Components (m, v, p));		
	}

	@Override
	public I_Presenter getPresenter(I_View v) {			
		for (int j=0; j < getComponents().size(); j++) {
			MVP_Components s = getComponents().get(j);

			if (s.get_view() == v) {
				return s.get_presenter();
			}
		}

		return null;
	}

	@Override
	public I_Model getModel(I_Presenter p) {
		for (int j=0; j < getComponents().size(); j++) {
			MVP_Components s = getComponents().get(j);

			if (s.get_presenter() == p) {
				return s.get_model();
			}
		}

		return null;
	}

	@Override
	public void unregisterMVPInstances(I_View v) throws Exception {
		for (int j=0; j < getComponents().size(); j++) {
			MVP_Components s = getComponents().get(j);

			if (s.get_view() == v) {
				getComponents().remove(j);
				return;
			}
		}

		throw new Exception ("view not found");	
	}
		
	// Getters/Setters -------------------------------------------------------
	
	private ArrayList<MVP_Components> getComponents() {
		return _components;
	}

}
