package view;

import mediator.I_View;

public interface I_CalculatorView extends I_View{
	public void display(String text);
	public void displayWarning(String text);
}
