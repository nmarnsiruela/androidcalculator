package presenter;

import static org.mockito.Mockito.verify;
import view.I_CalculatorView;

import model.I_CalculatorModel;

public class CalculatorPresenter implements I_CalculatorPresenter {

	private I_CalculatorModel _model;
	private I_CalculatorView  _view;

	//Almacena el n�mero y el operador
	private String _number;
	private String _savedOperand;
	private boolean _dotPressed;


	public CalculatorPresenter (I_CalculatorModel model, I_CalculatorView view) {
		set_model((I_CalculatorModel) model);
		set_view((I_CalculatorView) view);
		_number = "0";
		_savedOperand = "";
		_dotPressed = false;
		displayNumber();
	}

	//M�todo simple que muestra el n�mero actual.
	private void displayNumber() {
		get_view().display(_number);   
	}

	// ------------------------------------------------------------------------

	@Override
	public void backspacePressed() {
		_savedOperand = "";

		if (_number.length() == 1) {
			_number = "0";			
		} else {
			_number = StringUtils.remove_right_char(_number);	
		}

		displayNumber();
	}

	@Override
	public void clearPressed() {
		get_model().reset();

		_number = "0";
		_savedOperand = "";

		displayNumber();
	}

	@Override
	// TODO: Confirmar que esto est� bien.
	public void digitPressed(String c) {
		Integer MaxInt = Integer.MAX_VALUE;
		Integer MaxDigits = MaxInt.toString().length();
		
		if (_number.length() == MaxDigits) {
			;
		} else if (_number.equals("0")) {
			_number = c;
		} else {
			_number = _number + c;
		}
		displayNumber();
	}

	@Override
	public void operatorPressed (String c) {
		Float n;
		//  Evito problemas con numeros que tengan el maximo numero de cifras para
		//  un entero (por ejemplo 10 cifras), pero con un valor superior a Integer'Last

		try {
			n = StringUtils.toFloat(_number);
		} catch (Exception e) {
			get_view().displayWarning("wrong number");
			Float current_value =  get_model().getResult();
			_number = current_value.toString();
			displayNumber();
			_number = "0";
			return;
		}
		//Si no estoy realizando ninguna operaci�n y el acumulador se encuentra a 0, realizo un setter.
		if (_savedOperand.equals("")
				&& get_model().getResult() == 0) {
			get_model().setResult (n);

		} else {
			try {
				if (_savedOperand.equals("+")) {
					get_model().add(n);
					System.out.println(n + "<-- Suma");
				} else if (_savedOperand.equals("-")) {
					get_model().subtract(n);        		

				} else if (_savedOperand.equals("*")) {
					get_model().multiply(n);        		

				} else if (_savedOperand.equals("/")) {
					get_model().divide(n);
				}
			} catch (Exception e) {
				get_view().displayWarning("operation error");
			}
		}
		
		//Esto es la ejecuci�n del =, o cada vez que termino de operar, para mostrar el resultado
		Float current_value = get_model().getResult();
		if (_dotPressed == true){
			_number = current_value.toString();			
		}else{
			Integer int_value = current_value.intValue();
			_number = int_value.toString();
		}
		displayNumber();
		_number = "0";
		_savedOperand = c;
		_dotPressed = false;

	}




	@Override
	public void dotPressed() {
		if (_dotPressed == true){
			
		}else{
		_dotPressed = true;
		_number = _number + ".";
		displayNumber();
		}
	}

	//  Getters/setters ------------------------------------------------------

	protected I_CalculatorView get_view() {
		return _view;
	}

	private void set_view(I_CalculatorView _view) {
		this._view = _view;
	}

	// ---	

	private void set_model(I_CalculatorModel _model) {
		this._model = _model;
	}

	protected I_CalculatorModel get_model() {
		return _model;
	}

}