package presenter;

import mediator.I_Presenter;

public interface I_CalculatorPresenter extends I_Presenter{
	public void digitPressed(String c);
	public void operatorPressed (String c);
	
	public void clearPressed();
	public void backspacePressed();
	
	public void dotPressed();  	
}
