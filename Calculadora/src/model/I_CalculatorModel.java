package model;

import mediator.I_Model;

public interface I_CalculatorModel extends I_Model{
	
	public void add(float operand) throws Exception;
	public void subtract(float operand) throws Exception;
	public void multiply(float operand) throws Exception;
	public void divide(float operand) throws Exception;
	public void reset();

	public float  getResult();	
	public void setResult(float value);
}





