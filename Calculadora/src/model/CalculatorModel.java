package model;
public class CalculatorModel implements I_CalculatorModel {
	private float _result;
			
	public CalculatorModel() {
		_result = 0;
	}
	
	@Override
	public void add(float operand) throws Exception {
	    setResult (getResult() + operand);
	}

	@Override
	public void subtract(float operand) throws Exception {
	    setResult (getResult() - operand);
	}

	@Override
	public void multiply(float operand) throws Exception {
	    setResult (getResult() * operand);		
	}

	@Override
	public void divide(float operand) throws Exception {
		if (operand == 0) {
	         throw new DivideByZero();
		}
		
	    setResult (getResult() / operand);
	}
	
	@Override
	public float getResult() {
		return _result;
	}
	
	@Override
	public void setResult(float value) {
		_result = value;
	}

	@Override
	public void reset() {
		_result = 0;
	}

	
}
